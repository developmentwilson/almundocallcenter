/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.model.dao;

import java.sql.Date;

/**
 *
 * @author Wilson.Galindo
 */
public class EmployeeModel {
    private int id;
    private String name;
    private int employeetypeId;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the employeetypeId
     */
    public int getEmployeetypeId() {
        return employeetypeId;
    }

    /**
     * @param employeetypeId the employeetypeId to set
     */
    public void setEmployeetypeId(int employeetypeId) {
        this.employeetypeId = employeetypeId;
    }
    
}
