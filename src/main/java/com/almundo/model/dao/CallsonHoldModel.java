/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.model.dao;

import java.sql.Date;

/**
 *
 * @author Wilson.Galindo
 */
public class CallsonHoldModel {
    private int id;
    private Date dateattention;
    private Date dateregister;
    private String phonenumber;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the dateattention
     */
    public Date getDateattention() {
        return dateattention;
    }

    /**
     * @param dateattention the dateattention to set
     */
    public void setDateattention(Date dateattention) {
        this.dateattention = dateattention;
    }

    /**
     * @return the dateregister
     */
    public Date getDateregister() {
        return dateregister;
    }

    /**
     * @param dateregister the dateregister to set
     */
    public void setDateregister(Date dateregister) {
        this.dateregister = dateregister;
    }

    /**
     * @return the phonenumber
     */
    public String getPhonenumber() {
        return phonenumber;
    }

    /**
     * @param phonenumber the phonenumber to set
     */
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
    
    
    
}
