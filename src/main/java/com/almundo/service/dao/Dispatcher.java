/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.service.dao;

import com.almundo.model.dao.*;
import com.almundo.utility.MysqlConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Wilson.Galindo
 */
public class Dispatcher implements Runnable{
   
    public void run(){
         try{
             Thread.sleep(70000);
             
       }catch(Exception err){
            err.printStackTrace();
       }
    }
    
     public Dispatcher(String x) {
        dispatchCall(x);
    }

    
    //Método que consulta el empleado disponible en ese momento para
    //asignar la llamada.
    //Si no hay empleado disponible, la envia a la tabla callsonhold.
    public void dispatchCall(String phoneNumber)
    {
        
        EmployeeModel Emp = getEmployeeFree();
        if(Emp != null)
        {
            if(Emp.getId() > 0)
            {
                AssignedCallModel ass = new AssignedCallModel();
                ass.setEmployeeid(Emp.getId());
                ass.setPhonenumber(phoneNumber);

                createAsignedCall(ass);
            }
            else
            {
                freeEmployee();
                CallsonHoldModel ac = new CallsonHoldModel();
                ac.setPhonenumber(phoneNumber);
                createcallsonhold(ac);
            }
            
        }
        
    }
    
    //Actualiza fecha final, de la tabla assignedcall
    //Para indicar que el empleado ya esta disponible.
    public void freeEmployee()
    {
         MysqlConnect mysqlConnect = new MysqlConnect();
        try {
            java.util.Date today = new java.util.Date();
            
            
            String sql = "UPDATE `almundocallcenter`.`assignedcall` SET enddate=";
            
            
            
            sql = sql + '"' + new java.sql.Timestamp(today.getTime()).toString() + '"';
            
            
            
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            System.out.println("Call:");
           
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
               mysqlConnect.disconnect();
        }
    }
    
    //Crea un registro en la tabla callsonhold
    //Una vez a verificado que no hay ningun empleado disponible.
    public void createcallsonhold(CallsonHoldModel callsonhold)
    {
        MysqlConnect mysqlConnect = new MysqlConnect();
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement("INSERT INTO `almundocallcenter`.`callsonhold`(`dateregister`, `phoneNumber`) VALUES (?, ?)");
            System.out.println("Call:");
            java.util.Date today = new java.util.Date();
            statement.setTimestamp(1, new java.sql.Timestamp(today.getTime()));
            statement.setString(2, callsonhold.getPhonenumber());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
               mysqlConnect.disconnect();
        }
    }
    
    
    //Obtiene el empleado disponible en el momento de entrar la llamada
    public EmployeeModel getEmployeeFree()
    {
        EmployeeModel emp = new EmployeeModel();
        try
        {
            List<EmployeeModel> listemp = new ArrayList<EmployeeModel>();
            
            listemp = getEmployeeAll();
            
           if(listemp.size() > 0)
           {
               emp = listemp.get(0);
           }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return emp;
    }
    
   
    
    //Obtine la lista de empleado disponibles en el momento de 
    //entrar una llamada. la lista la devuelve ordenada 
    //por el numero de tipo de empleado de modo tal que se asignen deacuerdo
    //en orden 1=Operado, 2=Supervisor, 3=Director.
    public List<EmployeeModel> getEmployeeAll()
    {
        MysqlConnect mysqlConnect = new MysqlConnect();
        List<EmployeeModel> listemp = new ArrayList<EmployeeModel>();
        try
        {
            String sql = "SELECT e.Id, e.employeetypeid " +
                         "FROM almundocallcenter.employee e " +
                         "WHERE e.Id NOT IN " +
                         "(SELECT a.employeeid " +
                         "FROM almundocallcenter.assignedcall a " +
                         "WHERE a.enddate IS NULL) " +
                         "ORDER BY  e.employeetypeid";
            
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                EmployeeModel emp = new EmployeeModel();
                emp.setId(rs.getInt("id"));
                emp.setEmployeetypeId(rs.getInt("employeetypeid"));
                listemp.add(emp);
            }
            
            
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
               mysqlConnect.disconnect();
        }
        
        return listemp;
    }
    
    //Asigna la llamada al empleado
    public void createAsignedCall(AssignedCallModel assignedCall)
    {
        MysqlConnect mysqlConnect = new MysqlConnect();
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement("INSERT INTO `almundocallcenter`.`assignedcall`(`employeeid`, `startdate`, `phonenumber`) VALUES (?, ?, ?)");
            System.out.println("Call:");
            statement.setInt(1, assignedCall.getEmployeeid());
            java.util.Date today = new java.util.Date();
            statement.setTimestamp(2, new java.sql.Timestamp(today.getTime()));
            statement.setString(3, assignedCall.getPhonenumber());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
               mysqlConnect.disconnect();
        }
    }
    
    //Obtiene las lladas activas en el momento que ingresa una nueva
    //y a que empleado esta asignada.
    public List<AssignedCallModel> getCallAsignedActive()
    {
        MysqlConnect mysqlConnect = new MysqlConnect();
        List<AssignedCallModel> listcall = new ArrayList<AssignedCallModel>();
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement("SELECT * FROM assignedcall WHERE enddate is null");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                AssignedCallModel assigned = new AssignedCallModel();
                assigned.setId(rs.getInt("id"));
                assigned.setEmployeeid(rs.getInt("employeeid"));
                assigned.setStartdate(rs.getDate("startdate"));
                assigned.setEnddate(rs.getDate("enddate"));
                assigned.setPhonenumber(rs.getString("phonenumber"));
                listcall.add(assigned);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
               mysqlConnect.disconnect();
        }
        
        return listcall;
    }
    
}
